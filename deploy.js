var config = require('./config.json');
var request = require('request');

// rancher api uri for this service
var auth_host_uri = 'http://' + config.RANCHER_ACCESS_KEY + ':' + config.RANCHER_SECRET_KEY + '@' + config.RANCHER_HOST + ':' + config.RANCHER_PORT + '/';
var svc_url = auth_host_uri + 'v1/projects/' + config.PROJECT_ID + '/services/' + config.SERVICE_ID;

// retrieve launch configuration ( needed for upgrade )
function start_upgrade() {
    request(svc_url, function(error, response, body) {
        var svc = JSON.parse(body);
        var lc = svc.launchConfig;
        var slc = svc.secondaryLaunchConfigs;
        upgrade_service(lc, slc);
    });
}

// upgrade service
function upgrade_service(lc, slc) {
    console.log('###### STARTING UPGRADE');

    // request data
    var upgrade_url = svc_url + '/?action=upgrade';
    var req_body = {
        inServiceStrategy: {
            batchSize: 1,
            intervalMillis: 2000,
            startFirst: true,
            launchConfig: lc,
            secondaryLaunchConfigs: slc
        }
    };

    // make upgrade request
    var req = {url: upgrade_url, method: 'POST', json: req_body};
    request(req, function (error, response, body) {
        poll_upgrade();
    })
}

// upgrade completed
function finish_upgrade() {
    console.log('###### FINISHING UPGRADE');

    // request data
    var upgrade_url = svc_url + '/?action=finishupgrade';

    // make finish-upgrade request
    var req = {url: upgrade_url, method: 'POST', json: {}};
    request(req, function (error, response, body) {
        console.log('###### UPGRADE COMPLETE');
    })
}

// poll for upgrade to finish
function poll_upgrade() {
    console.log('###### WAITING FOR UPGRADE');

    request(svc_url, function(error, response, body) {
        var svc = JSON.parse(body);

        if (svc.state == 'upgrading') {
            setTimeout(poll_upgrade, 3000);
        } else {
            finish_upgrade();
        }
    });
}

// run
start_upgrade();